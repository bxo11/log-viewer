# Project LogViewer

Maven Java project

A new implementation of LogConsumer.
And a GUI to display logs


## Cloning

```
git clone git@github.com:tango-controls/LogViewer
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* log4j.jar
* JTango.jar
* ATKCore.jar
* ATKWidget.jar
  

#### Toolchain Dependencies 

* javac 8 or higher
* maven
  

### Build


Instructions on building the project.

```
cd LogViewer
mvn package
```

